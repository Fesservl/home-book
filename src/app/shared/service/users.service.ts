import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user.models';
import { map} from 'rxjs/operators';
import {BaseApi} from "../core/base-api";



@Injectable()
export class UsersService extends BaseApi{
  getUserByEmail(email: string): Observable<User> {
    return this.get(`users?email=${email}`).pipe(
      map((response: User[]) => response[0] ? response[0] : undefined )
    );
  }

  createNewUser(user: User):Observable<User> {
    return this.post(`users`,user)
  }



}
