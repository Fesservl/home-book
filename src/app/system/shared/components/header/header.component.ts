import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../../shared/service/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: 'wfm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  date:Date = new Date();
  name: string = JSON.parse(window.localStorage['user'])['name'];
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {

  }

  onLogOut(){
    this.authService.logout();
    this.router.navigate(['/login'])
  }

}
