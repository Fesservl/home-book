import {BaseApi} from "../../../shared/core/base-api";
import {Observable} from "rxjs";
import {Category} from "../models/category.model";

export class CategoriesService extends BaseApi{

  createNewCategory(category: Category):Observable<Category> {
    return this.post(`categories`,category)
  }
  getCategories():Observable<Category[]> {
    return this.get(`categories`)
  }
  updateCategory(category: Category):Observable<Category> {
    return this.put(`categories/${category.id}`,category)
  }
}
