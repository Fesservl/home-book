import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Bill} from "../models/bill.model";
import {map} from "rxjs/operators";
import {BaseApi} from "../../../shared/core/base-api";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class BillService extends BaseApi{
    constructor(public http: HttpClient){
      super(http);
    }

    getBill():Observable<Bill> {
      return this.get('bill')
    }

    getCurrency(base:string = 'RUB'):Observable<any>{
      return this.http.get(`http://data.fixer.io/api/latest?access_key=5a614450c491ad32f0cb470972aac520`).pipe(
        map(response => response)
      )
    }
}
