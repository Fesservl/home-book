import { Component, OnInit } from '@angular/core';
import {Category} from '../shared/models/category.model';
import {CategoriesService} from '../shared/service/categories.service';

@Component({
  selector: 'wfm-records-page',
  templateUrl: './records-page.component.html',
  styleUrls: ['./records-page.component.scss']
})
export class RecordsPageComponent implements OnInit {
  categories: Category[];
  isLoaded: boolean = false;
  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
    this.categoriesService.getCategories().subscribe((categories:Category[])=>{
      this.categories = categories;
      this.isLoaded = true;
    })
  }

  newCategoryAdded(category:Category){
    this.categories.push(category)
  }

  updateCategory(category:Category){
    this.categories.forEach((cat,i) => {
      if(cat.id === category.id){
        this.categories.splice(i,1,category)
      }
    })
  }

}
