import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Category} from '../../shared/models/category.model';
import {CategoriesService} from '../../shared/service/categories.service';
import {Message} from '../../../shared/models/message.models';

@Component({
  selector: 'wfm-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  @Input() categories: Category[] = [];
  @Output() onCategoryEdit = new EventEmitter<Category>();
  message: Message;
  currentCategoryId = 1;
  currentCategory:Category;
  constructor( private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.message = new Message('danger', '');
    this.onCategoryChange()
  }

  onSubmit(form:NgForm){

      let {name,capacity} = form.value;
      console.log(form)
      if(capacity < 0){capacity*=-1};
      let category = new Category(name, capacity,+this.currentCategoryId);
      console.log(category)
      this.categoriesService.updateCategory(category)
        .subscribe((category:Category)=>{
          this.onCategoryEdit.emit(category)
          this.message = new Message('success', 'Успешное редактирование');
          window.setTimeout(()=>{this.message.text ='' },3000)
        })
  }

  onCategoryChange=()=>{
    this.currentCategory = this.categories.find(cat => cat.id === +this.currentCategoryId)
  }

}
