import {NgModule } from '@angular/core';

import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AuthRoutingModule} from './auth-routing.module';
import {AuthComponent} from './auth.component';
import {SharedModule} from '../shared';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [ LoginComponent, RegistrationComponent, AuthComponent ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ],
  providers: []
})
export class AuthModule { }
