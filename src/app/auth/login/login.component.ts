import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../shared/service/users.service';
import {User} from '../../shared/models/user.models';
import {Message} from '../../shared/models/message.models';
import { AuthService } from '../../shared/service/auth.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'wfm-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;
  constructor(
    private userService: UsersService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) { }



  ngOnInit() {
    this.message = new Message('danger', '');
    this.route.queryParams
      .subscribe((params:Params)=>{
        if( params['nowCanLogin']){
           this.showMessage('success','Теперь вы можете зайти в систему')
        }
      });


    this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)])
    });
  }
  onSubmit() {
    const formData = this.form.value;
    this.userService.getUserByEmail(formData.email)
      .subscribe((user: User) => { if (user) {
        if ( user.password !== formData.password ) { return this.showMessage('danger', 'Неверный пароль');
        } else { 
          this.message.text = ''; 
          window.localStorage.setItem('user',JSON.stringify(user));
          this.authService.login();
          this.router.navigate(['/system','bill']);
        }
        } else {
          this.showMessage('danger', 'Нет такого пользователя');
        }
      });
  }
  private showMessage(type: string = 'danger', text: string) {
    this.message = new Message(type, text);
  }

}
