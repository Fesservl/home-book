import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared';
import {HttpClientModule} from '@angular/common/http';
import { AuthService } from './shared/service/auth.service';
import {SystemModule} from "./system/system.module";
import {UsersService} from "./shared/service/users.service";
import {BaseApi} from "./shared/core/base-api";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    AuthModule,
    AppRoutingModule,
    SystemModule
  ],
  providers: [AuthService,UsersService,BaseApi],
  bootstrap: [AppComponent]
})
export class AppModule { }
